package com.example.ter.robotblockly;

import android.annotation.SuppressLint;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends AppCompatActivity {

    @SuppressLint("JavascriptInterface")
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WebView wv=(WebView) this.findViewById(R.id.webview);
        wv.setWebViewClient(new WebViewClient());
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setDomStorageEnabled(true);
        wv.getSettings().setLoadWithOverviewMode(true);
        wv.getSettings().setUseWideViewPort(true);
        wv.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wv.setHorizontalScrollBarEnabled(true);
        wv.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        wv.getSettings().setAllowFileAccessFromFileURLs(true);
        wv.getSettings().setAllowUniversalAccessFromFileURLs(true);
        wv.getSettings().setAppCacheEnabled(true);
        wv.getSettings().setAllowFileAccess(true);
        wv.getSettings().setAllowContentAccess(true);
        wv.getSettings().setDatabaseEnabled(true);
        wv.getSettings().setDisplayZoomControls(true);
        wv.getSettings().setLoadsImagesAutomatically(true);
        wv.getSettings().setSupportMultipleWindows(true);
        wv.addJavascriptInterface(this, "android");

        if (savedInstanceState != null) {
            wv.restoreState(savedInstanceState);
        } else {
            String baseUrl = "file:///android_asset/blockly/index.html";
            wv.loadUrl("file:///android_asset/blockly/index.html");
        }

    }
}

